package sample;

import javafx.animation.*;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

import java.awt.event.ActionEvent;
import java.util.ArrayList;


public class Controller {
    @FXML
    Pane pane;

    GraphicsContext gc;

    ArrayList<Pet> pets;
    Wallpaper wallpaper;

    @FXML
    public void initialize(){
        pets = new ArrayList<Pet>();
        wallpaper = new Wallpaper(0,150);
        pets.add(new Cat(250,230));
        pets.add(new Bird(300,0));
        pets.add(new Dog(100,50));
        pets.add(new Rabbit(330,100));
        pets.add(new Turtle(-130,190));
    }
    @FXML
    public void display(){
        pane.getChildren().clear();

        wallpaper.fill();
        for (Pet pet : pets) {
            pet.fill();
        }
        pane.getChildren().add(wallpaper);
        pane.getChildren().addAll(pets);
    }

    @FXML
    public void fadeTransition(){
        for (Pet pet: pets) {
            doFade(pet);
        }
    }

    public void doFade(Canvas object) {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(300),object);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.1);
        fadeTransition.setCycleCount(100);
        fadeTransition.setAutoReverse(true);
        fadeTransition.play();
    }

    @FXML
    public void translateTransition() {
        int distance = 100;
        for (Pet pet: pets) {
            doTranslate(pet,distance);
            distance += 50;
        }
    }

    public void doTranslate(Canvas object, int distance) {
        TranslateTransition tt = new TranslateTransition(Duration.millis(3000), object);
        tt.setFromX(object.getTranslateX());
        tt.setToX(object.getTranslateX() + distance);
        tt.setCycleCount(10);
        tt.setAutoReverse(true);
        tt.play();
    }

    @FXML
    public void rotateTransition() {
        int angle = 360;
        for (Pet pet: pets) {
            doRotate(pet,angle);
            angle += 90;
            angle *= -1;
        }
    }

    public void doRotate(Canvas object, int angle) {
        RotateTransition rt = new RotateTransition(Duration.millis(3000), object);
        rt.setByAngle(angle);
        rt.setCycleCount(10);
        rt.setAutoReverse(true);
        rt.play();
    }

    @FXML
    public void scaleTransition() {
        int scale = 1;
        for (Pet pet: pets) {
            doScale(pet,scale++);
        }
    }

    public void doScale(Canvas object, int size) {
        ScaleTransition st = new ScaleTransition(Duration.millis(3000), object);
        st.setToX(size);
        st.setToY(size);
        st.setCycleCount(4);
        st.setAutoReverse(true);
        st.play();
    }

    @FXML
    public void pathTransition() {
        int scale = 1;
        for (Pet pet: pets) {
            doPath(pet,(int)Math.random()*800,(int)Math.random()*600);
        }
    }

    public void doPath(Canvas object,int x,int y){
        Path path = new Path();
        path.getElements().add(new MoveTo(x, y));
        path.getElements().add(new CubicCurveTo(380, 0, 380, 120, 200, 120));
        path.getElements().add(new CubicCurveTo(0, 120, 0, 240, 380, 240));
        PathTransition pt = new PathTransition();
        pt.setDuration( javafx.util.Duration.millis(4000));
        pt.setPath(path);
        pt.setNode(object);
        pt.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pt.setCycleCount(Timeline.INDEFINITE);
        pt.setAutoReverse(true);
        pt.play();
    }
}
