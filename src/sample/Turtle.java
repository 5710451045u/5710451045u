package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Turtle extends Pet {
    Turtle(int x, int y){
        super(x,y);
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;
        gc.setFill(c = Color.rgb(163, 255, 117));

        gc.fillOval(this.getTranslateX()+445, this.getTranslateY()-80 ,40, 50); //หัว

        gc.fillRect(this.getLayoutX()+350,this.getLayoutY()+170,30,10);//แขน
        gc.fillRect(this.getLayoutX()+290,this.getLayoutY()+170,30,10); //แขน
        gc.fillRect(this.getLayoutX()+320,this.getLayoutY()+230,10,30); //ขา
        gc.fillRect(this.getLayoutX()+345,this.getLayoutY()+230,10,30); //ขา

        gc.setFill(c = Color.rgb(4, 204, 7));

        gc.fillOval(this.getLayoutX()+310,this.getLayoutY()+155,55,80); //ตัว
        gc.fillOval(this.getTranslateX()+460, this.getTranslateY()-55 ,3, 10); //ตา
        gc.fillOval(this.getTranslateX()+470, this.getTranslateY()-55 ,3, 10); //ตา
        gc.fillOval(this.getTranslateX()+460, this.getTranslateY()-70 ,15, 1); //ปาก
    }

}
