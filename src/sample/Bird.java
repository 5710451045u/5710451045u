package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Bird extends Pet {
    Bird(int x, int y){
        super(x,y);
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;
        gc.setFill(c = Color.rgb(255, 252, 85));

        gc.fillOval(this.getTranslateX(), this.getTranslateY() ,40, 40);//หัว
        gc.fillOval(this.getTranslateX()+5, this.getTranslateY()+30 ,26, 35);//ตัว
        gc.fillOval(this.getTranslateX()-15, this.getTranslateY()+40 ,20, 10);//ปีก
        gc.fillOval(this.getTranslateX()+30, this.getTranslateY()+40 ,20, 10);//ปีก

        gc.setFill(c = Color.rgb(245, 204, 0));
        gc.fillOval(this.getTranslateX()+10, this.getTranslateY()+20 ,20, 10);//ปาก
        gc.fillOval(this.getTranslateX()+10, this.getTranslateY()+10 ,2, 7);//ตา
        gc.fillOval(this.getTranslateX()+25, this.getTranslateY()+10 ,2, 7);//ตา

        gc.fillRect(this.getTranslateX(), this.getTranslateY()+60 ,10, 3);//ขา
        gc.fillRect(this.getTranslateX()+25, this.getTranslateY()+60 ,10, 3);//ขา
    }

}
