package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rabbit extends Pet{
    Rabbit(int x, int y){
        super(x,y);
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;
        gc.setFill(c = Color.rgb(238, 156, 255));

        gc.fillOval(this.getTranslateX()-50, this.getTranslateY() ,70, 55); //หัว
        gc.fillOval(this.getTranslateX()-40, this.getTranslateY()+40 ,50, 60); //ตัว
        gc.fillOval(this.getTranslateX()-30, this.getTranslateY()-40 ,15, 55); //หู
        gc.fillOval(this.getTranslateX()-10, this.getTranslateY()-40 ,15, 55); //หู

        gc.fillOval(this.getTranslateX()-55, this.getTranslateY()+50 ,20, 10); //แขน
        gc.fillOval(this.getTranslateX()+5, this.getTranslateY()+50 ,20, 10); //แขน
        gc.fillOval(this.getTranslateX()-30, this.getTranslateY()+95 ,10, 20); //ขา
        gc.fillOval(this.getTranslateX()-5, this.getTranslateY()+95 ,10, 20); //ขา

        gc.setFill(c = Color.rgb(177, 86, 255));

        gc.fillOval(this.getTranslateX()-25, this.getTranslateY()-30 ,5, 30); //หู
        gc.fillOval(this.getTranslateX()-5, this.getTranslateY()-30 ,5, 30); //หู

        gc.fillOval(this.getTranslateX()-10, this.getTranslateY()+20 ,3, 10); //ตา
        gc.fillOval(this.getTranslateX()-20, this.getTranslateY()+20 ,3, 10); //ตา
        gc.fillOval(this.getTranslateX()-20, this.getTranslateY()+40 ,15, 2); //ปาก
    }

}
