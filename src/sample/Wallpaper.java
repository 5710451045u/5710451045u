package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Wallpaper extends Canvas {
    Wallpaper(int x, int y){
        setTranslateX(x);
        setTranslateY(y);

        setWidth(800); //ขอบเขตของรูปที่วาด
        setHeight(600);  //ขอบเขตของรูปที่วาด
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;

        gc.setFill(c = Color.rgb(213, 10, 255));
        gc.fillRect(this.getTranslateX(), this.getTranslateY()-300 ,800, 600);//ฟ้า

        gc.setFill(c = Color.rgb(234, 249, 192));
        gc.fillRect(this.getTranslateX(), this.getTranslateY()-300 ,800, 600);//ดิน




        gc.setFill(c = Color.rgb(200, 240, 255));

        gc.fillOval(this.getTranslateX(), this.getTranslateY() ,70, 50);//น้ำ
        gc.fillOval(this.getTranslateX()-50, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+50, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+100, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+150, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+200, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+250, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+300, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+350, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+400, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+450, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+500, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+550, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+600, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+650, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+700, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+750, this.getTranslateY() ,70, 50);
        gc.fillOval(this.getTranslateX()+800, this.getTranslateY() ,70, 50);

        gc.setFill(c = Color.rgb(173, 209, 255));
        gc.fillOval(this.getTranslateX()-25, this.getTranslateY()+40 ,70, 50);//น้ำ
        gc.fillOval(this.getTranslateX()+25, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+75, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+125, this.getTranslateY()+40,70, 50);
        gc.fillOval(this.getTranslateX()+175, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+225, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+275, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+325, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+375, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+425, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+475, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+525, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+575, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+625, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+675, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+725, this.getTranslateY()+40 ,70, 50);
        gc.fillOval(this.getTranslateX()+775, this.getTranslateY()+40 ,70, 50);


        gc.setFill(c = Color.rgb(116, 232, 0));
        gc.fillRect(this.getTranslateX()+230, this.getTranslateY()+180 ,2, 50);//หญ้า
        gc.fillRect(this.getTranslateX()+235, this.getTranslateY()+180 ,2, 50);
        gc.fillRect(this.getTranslateX()+240, this.getTranslateY()+180 ,2, 50);
        gc.fillRect(this.getTranslateX()+245, this.getTranslateY()+180 ,2, 50);
        gc.fillRect(this.getTranslateX()+250, this.getTranslateY()+180 ,2, 50);
        gc.fillRect(this.getTranslateX()+255, this.getTranslateY()+180 ,2, 50);
        gc.fillRect(this.getTranslateX()+260, this.getTranslateY()+180 ,2, 50);

        gc.fillRect(this.getTranslateX()+700, this.getTranslateY()+65 ,2, 50);//หญ้า
        gc.fillRect(this.getTranslateX()+705, this.getTranslateY()+65 ,2, 50);
        gc.fillRect(this.getTranslateX()+710, this.getTranslateY()+65 ,2, 50);
        gc.fillRect(this.getTranslateX()+715, this.getTranslateY()+65 ,2, 50);
        gc.fillRect(this.getTranslateX()+720, this.getTranslateY()+65 ,2, 50);
        gc.fillRect(this.getTranslateX()+725, this.getTranslateY()+65 ,2, 50);
        gc.fillRect(this.getTranslateX()+730, this.getTranslateY()+65 ,2, 50);

        gc.fillRect(this.getTranslateX()+85, this.getTranslateY()-80 ,2, 50);//หญ้า
        gc.fillRect(this.getTranslateX()+90, this.getTranslateY()-80 ,2, 50);
        gc.fillRect(this.getTranslateX()+95, this.getTranslateY()-80 ,2, 50);
        gc.fillRect(this.getTranslateX()+100, this.getTranslateY()-80 ,2, 50);
        gc.fillRect(this.getTranslateX()+105, this.getTranslateY()-80 ,2, 50);


        gc.fillRect(this.getTranslateX()+555, this.getTranslateY()-150 ,2, 50);//หญ้า
        gc.fillRect(this.getTranslateX()+560, this.getTranslateY()-150 ,2, 50);
        gc.fillRect(this.getTranslateX()+565, this.getTranslateY()-150 ,2, 50);
        gc.fillRect(this.getTranslateX()+570, this.getTranslateY()-150 ,2, 50);
        gc.fillRect(this.getTranslateX()+575, this.getTranslateY()-150 ,2, 50);
        gc.fillRect(this.getTranslateX()+580, this.getTranslateY()-150 ,2, 50);
        gc.fillRect(this.getTranslateX()+585, this.getTranslateY()-150 ,2, 50);

    }

}
