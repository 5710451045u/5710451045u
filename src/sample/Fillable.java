package sample;

public interface Fillable {
    public void fill();
}
