package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Dog extends Pet{
    Dog(int x, int y){
        super(x,y);
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;
        gc.setFill(c = Color.rgb(255, 199, 173));

        gc.fillOval(this.getTranslateX()-5, this.getTranslateY() ,70, 70);//หัว
        gc.fillRect(this.getTranslateX()+50, this.getTranslateY()+40 ,90, 45);//ตัว
        gc.fillOval(this.getTranslateX()+135, this.getTranslateY()+5 ,10, 50);//หาง

        gc.fillRect(this.getLayoutX()+150,this.getLayoutY()+120,10,50); //ขา
        gc.fillRect(this.getLayoutX()+170,this.getLayoutY()+120,10,50); //ขา
        gc.fillRect(this.getLayoutX()+210,this.getLayoutY()+120,10,50); //แขน
        gc.fillRect(this.getLayoutX()+230,this.getLayoutY()+120,10,50); //แขน

        gc.setFill(c = Color.rgb(255, 172, 129));

        gc.fillOval(this.getTranslateX()+5, this.getTranslateY()+30 ,50, 35);//จมูก

        gc.setFill(c = Color.rgb(255, 100, 80));

        gc.fillOval(this.getTranslateX()+15, this.getTranslateY()+10 ,5, 15);//ตา
        gc.fillOval(this.getTranslateX()+35, this.getTranslateY()+10 ,5, 15);//ตา
        gc.fillOval(this.getTranslateX()+20, this.getTranslateY()+55 ,15, 3);//ปาก
        gc.fillOval(this.getTranslateX()+50, this.getTranslateY()+10 ,20,40);//หู
        gc.fillOval(this.getTranslateX()-10, this.getTranslateY()+10 ,20, 40);//หู

    }

}
