package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Cat extends Pet{
    Cat(int x, int y){
        super(x,y);
    }

    public void fill() {
        GraphicsContext gc = getGraphicsContext2D();
        Color c ;
       gc.setFill(c = Color.rgb(255, 187, 89));

       gc.fillOval(this.getTranslateX(), this.getTranslateY() ,75, 75); //หัว
       gc.fillRect(this.getLayoutX()+190,this.getLayoutY()+210,80,50); //ตัว
       gc.fillRect(this.getLayoutX()+290,this.getLayoutY()+293,25,20); //หู
       gc.fillRect(this.getLayoutX()+313,this.getLayoutY()+270,20,25); //หู

       gc.fillRect(this.getLayoutX()+160,this.getLayoutY()+210,35,10); //ขา
       gc.fillRect(this.getLayoutX()+160,this.getLayoutY()+250,35,10); //ขา
        gc.fillRect(this.getLayoutX()+235,this.getLayoutY()+250,10,35); //แขน
        gc.fillRect(this.getLayoutX()+260,this.getLayoutY()+210,35,10); //แขน
        gc.fillRect(this.getLayoutX()+200,this.getLayoutY()+250,10,55); //หาง

        gc.setFill(c = Color.rgb(255, 103, 0));

        gc.fillOval(this.getTranslateX()+55, this.getTranslateY()+25 ,6, 15); //ตา
        gc.fillOval(this.getTranslateX()+30, this.getTranslateY()+50 ,15, 6); //ตา
        gc.fillOval(this.getTranslateX()+20, this.getTranslateY()+20 ,15, 15); //ปาก

    }

}
