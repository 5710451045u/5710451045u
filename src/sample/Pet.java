package sample;

import javafx.scene.canvas.Canvas;

public abstract class Pet extends Canvas implements Fillable{

    Pet(int x, int y){
        setTranslateX(x);
        setTranslateY(y);

        setWidth(800); //ขอบเขตของรูปที่วาด
        setHeight(600);  //ขอบเขตของรูปที่วาด
    }

}
